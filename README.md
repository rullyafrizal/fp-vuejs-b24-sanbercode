# blog-project

## Tugas Sanbercode Link Below

- https://youtu.be/dlLAzE_OXyk
- https://drive.google.com/drive/folders/1jbov59FXgZ2swdBMASEytNufbJ-QxYrf?usp=sharing

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
